# Generated by Django 2.2.3 on 2019-08-02 09:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False, unique=True)),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('preview', models.TextField(verbose_name='Анонс новости')),
                ('text', models.TextField(verbose_name='Текс новости')),
                ('created_datetime', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_datetime', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('is_published', models.BooleanField(default=False, verbose_name='Статус публикаций')),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField(verbose_name='Текст комментария')),
                ('created_datetime', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_datetime', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='post.Post', verbose_name='Новость')),
            ],
        ),
    ]
