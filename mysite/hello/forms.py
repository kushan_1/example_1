from django import forms


class SumForm(forms.Form):
    a = forms.IntegerField(widget=forms.NumberInput(attrs={'styles': 'width:40px'}))
    b = forms.IntegerField(widget=forms.NumberInput(attrs={'styles': 'width:40px'}))
    c = forms.IntegerField(widget=forms.NumberInput(attrs={'styles': 'width:40px'}))

    def clean_a(self):
        a = self.cleaned_data['a']
        if a and a == 0:
            raise forms.ValidationError('a Не может равнятся нулю')
        return a
