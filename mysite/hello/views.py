from math import sqrt

from django.http import HttpResponse
from django.shortcuts import render

from hello.forms import SumForm


def first_run(request):
    if request.POST:
        form = SumForm(request.POST)
        if form.is_valid():
            a = form.cleaned_data['a']
            b = form.cleaned_data['b']
            c = form.cleaned_data['c']

            D = (int(b)**2)-4*int(a)*int(c)
            if D > 0:
                x1 = (-b - sqrt(D))/(2*a)
                x2 = (-b + sqrt(D))/(2*a)
                return render(request, 'index.html', {
                    'form': SumForm(), 'succes': True, 'a': a, 'b': b, 'c': c, 'D': D, 'x1': x1, 'x2': x2
                })
            elif D == 0:
                x = -b/2*a
                return render(request, 'index.html', {
                    'form': SumForm(), 'succes': True, 'a': a, 'b': b, 'c': c, 'D': D, 'x': x
                })
            else:
                D = str(D) + ' ' + '(Так как дискриминант меньше нуля, то уравнение не имеет действительных решений.)'
                return render(request, 'index.html', {
                    'form': SumForm(), 'succes': True, 'a': a, 'b': b, 'c': c, 'D': D
                })

        else:
            return render(request, 'index.html', {'form': form})

    return render(request, 'index.html', {'form': SumForm()})
