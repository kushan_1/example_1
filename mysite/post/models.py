from django.db import models
from django.utils.translation import ugettext_lazy as _


class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(_('Заголовок'), max_length=255)
    preview = models.TextField(_('Анонс новости'))
    image = models.ImageField(
        _('Изображение'), upload_to='post/images/', null=True, blank=True
    )
    text = models.TextField(_('Текст новости'))
    created_datetime = models.DateTimeField(
        _('Дата создания'), auto_now_add=True
    )
    updated_datetime = models.DateTimeField(_('Дата обновления'), auto_now=True)
    is_published = models.BooleanField(_('Статус публикации'), default=False)

    class Meta:
        verbose_name = _('Новость')
        verbose_name_plural = _('Новости')
        ordering = ('-created_datetime',)

    def __str__(self):
        return 'Новость:{0}'.format(self.title)


class Comment(models.Model):
    comment = models.TextField(_('Текст комментария'))
    created_datetime = models.DateTimeField(
        _('Дата создания'), auto_now_add=True
    )
    updated_datetime = models.DateTimeField(
        _('Дата обновления'), auto_now=True
    )
    post = models.ForeignKey(
        Post, verbose_name=_('Новость'), on_delete=models.CASCADE,
        related_name='comments'
    )
    parent = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.SET_NULL,
        related_name='answer'
    )

    class Meta:
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')
        ordering = ('-created_datetime',)

    def __str__(self):
        return 'Комментарий к {0}'.format(self.post.title)
