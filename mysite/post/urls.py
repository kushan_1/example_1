from django.urls import path

from post.views import (
    PostDeleteView, PostListView, PostDetailView, PostCreateView,
    PostUpdateView, AnswerCommentCreateView
)

urlpatterns = [
    path('detail/<int:post_id>/', PostDetailView.as_view(), name='post_detail'),
    path('edit/<int:pk>/', PostUpdateView.as_view(), name='post_edit'),
    path('create/<int:comment_id>/', AnswerCommentCreateView.as_view(), name='answer_comment'),
    path('delete/<int:pk>/', PostDeleteView.as_view(), name='post_delete'),
    path('create/', PostCreateView.as_view(), name='post_create'),
    path('', PostListView.as_view(), name='main'),
]
