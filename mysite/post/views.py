from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, \
    DeleteView

from post.forms import CreatePostForm, EditPostForm
from post.models import Post, Comment


def post_list(request):
    context = {'object_list': Post.objects.filter(is_published=True)}
    return render(request, 'post/list.html', context)


def post_detail(request, pk):
    obj = get_object_or_404(Post, pk=pk)
    context = {'object': obj}
    return render(request, 'post/detail.html', context)


def post_create(request):
    context = {}
    if request.POST:
        form = CreatePostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.is_published = True
            post.save()
            return HttpResponseRedirect(reverse_lazy('main'))
        else:
            context.update({'form': form})
    else:
        context.update({'form': CreatePostForm})
    return render(request, 'post/create.html', context)


def post_edit(request, pk):
    obj = get_object_or_404(Post, pk=pk)
    context = {}
    if request.POST:
        form = EditPostForm(instance=obj, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse_lazy('post_detail', args=[pk])
            )
        else:
            context.update({'form': form})
    else:
        context.update({'form': EditPostForm(instance=obj)})
    return render(request, 'post/create.html', context)


def post_delete(request, pk):
    obj = get_object_or_404(Post, pk=pk)
    context = {}
    if request.POST:
        obj.delete()
        return HttpResponseRedirect(reverse_lazy('main'))
    return render(request, 'post/delete.html', context)


class PostListView(ListView):
    queryset = Post.objects.filter(is_published=True)
    template_name = 'post/list.html'
    paginate_by = 3
    context_object_name = 'test'

    def get_queryset(self):
        return Post.objects.all()


class PostDetailView(DetailView):
    queryset = Post.objects.all()
    template_name = 'post/detail.html'
    pk_url_kwarg = 'post_id'

    def get_context_data(self, **kwargs):
        data = super(PostDetailView, self).get_context_data(**kwargs)
        data['hello'] = 'Привет юзер!'
        return data


class PostCreateView(CreateView):
    queryset = Post.objects.all()
    form_class = CreatePostForm
    template_name = 'post/create.html'

    def get_success_url(self):
        return reverse_lazy('main')

    def form_valid(self, form):
        post = form.save(commit=False)
        post.is_published = True
        post.save()
        return HttpResponseRedirect(self.get_success_url())


class PostUpdateView(UpdateView):
    queryset = Post.objects.all()
    template_name = 'post/create.html'
    success_url = '/'
    form_class = EditPostForm

    def get_success_url(self):
        return reverse_lazy('post_detail', args=[self.kwargs['pk']])


class PostDeleteView(DeleteView):
    template_name = 'post/delete.html'
    queryset = Post.objects.all()

    def get_success_url(self):
        return reverse_lazy('main')


class AnswerCommentCreateView(CreateView):
    template_name = 'post/create.html'
    queryset = Comment.objects.all()
    fields = ('comment',)

    def get_success_url(self):
        comment_id = self.kwargs['comment_id']
        comment = Comment.objects.get(id=comment_id)
        return reverse_lazy('post_detail', args=[comment.post_id])

    def form_valid(self, form):
        answer = form.save(commit=False)
        comment_id = self.kwargs['comment_id']
        comment = Comment.objects.get(id=comment_id)
        answer.parent_id = comment.id
        answer.post_id = comment.post_id
        answer.save()
        return HttpResponseRedirect(self.get_success_url())
