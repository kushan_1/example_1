import requests
from bs4 import BeautifulSoup

url = 'http://www.psychologies.ru/articles/'

response = requests.get(url)

data = []

if response.status_code == 200:
    soup = BeautifulSoup(response.text, 'html.parser')
    for div in soup.findAll('div', class_="row-container rubric-anons"):
        records = {}
        for title in div.findAll('a'):
            records['title'] = title.text
        for p in div.findAll('div'):
            records['text'] = p.text

        with open('ithapens1.txt', 'a', encoding='utf8') as fp:
            fp.write(records['title'])
            fp.write('\n')
            fp.write(records['text'])
            fp.write('\n')
            fp.write('='*50)
            fp.write('\n')